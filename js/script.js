//#### Left Sideber

//Star Toggling
$( ".star" ).click(function() {
  $( this ).toggleClass( "active" );
});

//Show/Hide Header Toggling
$(document).ready(function() {
    $(".sidebar-section").hide();
    $(".sidebar-section").show();
    $(".sidebar-heading").click(function(){
            $(this).next(".sidebar-section").slideToggle(500);
            $(this).find(".sidebar-heading").toggle();
    });
});

//#### Middle Content

//Show/Hide Header Odds Table
$(document).ready(function() {
    $(".inner-background").show();
    $(".arrow-up").hide();
    $(".show-hide-header").click(function(){
            $(this).next(".inner-background").slideToggle(500);
            $(this).find(".arrow-up, .arrow-down").toggle();
    });
});

//#### Right Sidebar

//Close Chosen Odd
$(document).ready(function(c) {
    $('.alert-close').on('click', function(c){
        $(this).parent().fadeOut('slow', function(c){
        });
    });
});

//Close All Chosen Odds
$( ".alert-close.all" ).click(function() {
  $( ".option, .confirmation" ).hide( "slow" );
});
